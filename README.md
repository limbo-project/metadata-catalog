## Welcome to the LIMBO metadata catalog repository

[Here](https://gitlab.com/limbo-project/metadata-catalog/raw/master/catalog.all.ttl) you can access/download the whole catalog as a single RDF file.

This file is updated on the master branch using gitlab-ci after any chance in the master's toLoad folder.

![Limbo Cloud](https://gitlab.com/limbo-project/metadata-catalog/raw/master/limbo.svg)

## Accessing data using sparql-integrate and jq

* [sparql-integrate](https://github.com/SmartDataAnalytics/Sparqlintegrate) is a nifty command line tool that for working with RDF and SPARQL.

Setup using the debian package:
```bash
wget -qO - http://cstadler.aksw.org/repos/apt/conf/packages.precise.gpg.key  | sudo apt-key add 
echo 'deb http://cstadler.aksw.org/repos/apt precise main contrib non-free' | sudo tee -a /etc/apt/sources.list.d/cstadler.aksw.org.list
sudo apt update
sudo apt install sparql-integrate-cli
```

* jq is a powerful command line json processor


## Referencing LIMBO datasets

The following statement lists information about all datasets in the dataset catalog as JSON.
(Note: the prefixes.ttl file currently resides in the dataset-includes repository)

```
sparql-integrate catalog.all.ttl <(echo 'SELECT DISTINCT ?s { ?s a dcat:Dataset ; eg:groupId "org.limbo" }') --jq | jq '.[].s'
```


## Useful references

* Get all downloadURLs of all datasets

```bash
sparql-integrate catalog.all.ttl <(echo 'SELECT DISTINCT ?s { ?s a dcat:Dataset }') --jq | jq '.[].s.distribution[].downloadURL[]'
```

* Get the latest version of an artifact

```bash
sparql-integrate catalog.all.ttl <(echo 'SELECT ?s { ?s eg:groupId "org.limbo" ; eg:artifactId "train_1" FILTER(!EXISTS { ?x eg:priorVersion ?s }) }')
```


SELECT DISTINCT ?s { ?s a dcat:Dataset ; todo:groupId 'org.limbo' }
todo: make namespace stable


TODO Establish a link between dataset repository and link repository.

TODO Renaming von namespaces in limbo datensets
TODO Automatisches version bumping in dataset / linkset release
TODO Link repositories erstellen
TODO Raw data mapping status aus dem confluence in den katalog release


