# Link Specifications for Silk

## Execution

```
java -DconfigFile=<name>.xml -jar silk.jar
```

## Specifications to output

* dcterms-license_linkSpec.xml - describedLicenses.n3
* sameAs_linkSpec.xml - sameAs.n3
* weakSameAs_linkSpec.xml - weakSameAs.n3

## Info

The objective is to link DALICC licenses to the existing licenses of the metadata of mcloud.
This is sameAs.n3 .
License names have synonyms which are weakSameAs.n3 .
Not all license information in the mcloud metadata is described as a license document and linked in the dataset via dcterms:license, but hints to the license in the description.
The name and alternative names of a DALICC license could be matched with the ones in the description.
At the moment this match does not work but will be listed in describedLicenses.n3 .
